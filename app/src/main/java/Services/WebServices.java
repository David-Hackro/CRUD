package Services;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.hackro.database.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Entities.Persona;
import Interfaces.IDataConection;
import Interfaces.IWebServices;

/**
 * Created by hackro on 26/12/15.
 */
public class WebServices implements IWebServices, IDataConection {

    private Context contexto;
    private String result = "";

    public WebServices(Context contexto) {
        this.contexto = contexto;
    }

    @Override
    public void Insert(final Persona persona) {
        final RequestQueue queue = Volley.newRequestQueue(contexto);
        final String url = Ip + "" + Port + "" + WmInsert;//"http://10.0.3.2/CRUD/Insert.php";
        //Ip + "" + Port + "" + WmInsert
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                    Toast.makeText(contexto, R.string.MensajeExitoso, Toast.LENGTH_SHORT).show();
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override //Esta seccion es para el paso de parametros
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nombre", String.valueOf(persona.getNombre()));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void Delete(final int id) {
        final RequestQueue queue = Volley.newRequestQueue(contexto);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ip + ":" + Port + "/" + WmDelete, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(contexto, R.string.MensajeExitoso+response, Toast.LENGTH_SHORT).show();
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto, R.string.MensajeFallido, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override //Esta seccion es para el paso de parametros
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_usuario", String.valueOf(id));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void Update(final Persona persona) {
        final RequestQueue queue = Volley.newRequestQueue(contexto);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ip + ":" + Port + "/" + WmUpdate, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Toast.makeText(contexto, R.string.MensajeExitoso, Toast.LENGTH_SHORT).show();
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto, R.string.MensajeFallido, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override //Esta seccion es para el paso de parametros
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_usuario", String.valueOf(persona.getId()));
                params.put("nombre", persona.getNombre());
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public void Select() {
        final RequestQueue queue = Volley.newRequestQueue(contexto);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Ip + "" + Port + "" + WmSelect, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Toast.makeText(contexto, response, Toast.LENGTH_SHORT).show();
            }
        }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto, R.string.MensajeFallido, Toast.LENGTH_SHORT).show();
            }
        });
        queue.add(stringRequest);
    }


}
