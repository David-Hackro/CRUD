package Interfaces;

import java.util.List;

import Entities.Persona;

/**
 * Created by hackro on 26/12/15.
 */
public interface IWebServices {


    public void Insert(Persona persona);
    public void Delete(int id);
    public void Update(Persona persona);
    public void Select();


}
