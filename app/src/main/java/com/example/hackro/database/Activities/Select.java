package com.example.hackro.database.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.hackro.database.R;

import Entities.Persona;
import Services.WebServices;

public class Select extends AppCompatActivity {

    private WebServices servicios;
    private EditText Nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        servicios = new WebServices(Select.this);
        Nombre  = (EditText) findViewById(R.id.nombre);
    }

    public void SelectPersona(View view){
        servicios.Select();
    }
}
