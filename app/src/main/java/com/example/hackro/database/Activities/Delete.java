package com.example.hackro.database.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.hackro.database.R;

import Entities.Persona;
import Services.WebServices;

public class Delete extends AppCompatActivity {

    private WebServices servicios;
    private EditText id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);

        servicios = new WebServices(Delete.this);
        id  = (EditText) findViewById(R.id.id);
    }

    public void DeletePersona(View view){
        servicios.Delete(Integer.valueOf(id.getText().toString()));
    }
}
