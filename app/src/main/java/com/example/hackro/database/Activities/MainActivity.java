package com.example.hackro.database.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.hackro.database.R;

import Entities.Persona;
import Services.WebServices;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void Insertar(View v)
    {
        startActivity(new Intent(this,Insert.class));
    }

    public void Delete(View v)
    {
        startActivity(new Intent(this,Delete.class));
    }

    public void Update(View v)
    {
        startActivity(new Intent(this,Update.class));
    }

    public void Select(View v)
    {
        startActivity(new Intent(this,Select.class));
    }


}
