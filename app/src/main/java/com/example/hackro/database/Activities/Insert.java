package com.example.hackro.database.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.hackro.database.R;

import Entities.Persona;
import Services.WebServices;

public class Insert extends AppCompatActivity {

    private WebServices servicios;
    private EditText Nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        servicios = new WebServices(Insert.this);
        Nombre  = (EditText) findViewById(R.id.nombre);
    }

    public void InsertarPersona(View view){
            servicios.Insert(new Persona(0,Nombre.getText().toString()));
    }
}
