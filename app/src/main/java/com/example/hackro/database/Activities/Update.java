package com.example.hackro.database.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.hackro.database.R;

import Entities.Persona;
import Services.WebServices;

public class Update extends AppCompatActivity {

    private WebServices servicios;
    private EditText Nombre,id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        servicios = new WebServices(Update.this);
        id = (EditText) findViewById(R.id.id);
        Nombre  = (EditText) findViewById(R.id.nombre);
    }

    public void UpdatePersona(View view){
        servicios.Update(new Persona(Integer.valueOf(id.getText().toString()),Nombre.getText().toString()));
    }
}
